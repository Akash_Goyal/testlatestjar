
import com.hilabs.analytics.spark.ml.association_mining.DataCorresToRules
import com.hilabs.analytics.spark.ml.auto_mapping.Util
import com.hilabs.dao.impl.MysqlUpdate
import com.hilabs.model.{AssociationModelDetails, Datasets, Input, ModelParameters}
import com.hilabs.service.impl.{AssociationModelDetailsServiceImpl, DatasetsServiceImpl, ScenarioDetailsServiceImpl}
import org.apache.log4j.Logger
import com.hilabs.products.utils._
import com.hilabs.util.generic.ScalaGenericUtility
import com.hilabs.products.process_validation.model_type._
import com.hilabs.products.process_validation.utils.ProcessValidationParameters
import com.hilabs.products.utils.SparkConf
import com.hilabs.analytics.spark.ml.auto_mapping.Util
import com.hilabs.model.{AssociationModelDetails, ModelParameters}
import org.apache.spark.sql._
import com.hilabs.products.process_validation.results.{ProcessValidationResults, _}
import com.hilabs.products.process_validation.utils.ProcessValidationParameters
import com.hilabs.products.process_validation.utils.generic_functions._
import com.hilabs.products.utils.SparkConf
import org.apache.spark.sql.functions._
import com.hilabs.products.utils.FileUtils
import org.apache.log4j.Logger
import com.hilabs.service.impl.AssociationModelFlagsServiceImpl
import com.hilabs.products.process_validation.model_features.GetFeatureInfo
import scala.util.control.Breaks.{break, breakable}
import scala.collection.JavaConversions._
import scala.collection.JavaConverters._
import util.control.Breaks._
import com.hilabs.util.file_format_handler.EditDistance
import org.apache.spark.storage.StorageLevel
import com.hilabs.products.process_validation.utils.results.CustomRulesUtil
import com.hilabs.config.HilabsConfig;
import com.hilabs.dao.impl.MysqlUpdate;
import com.hilabs.dao.util.DatabaseConnector;
import com.hilabs.analytics.spark.ml.auto_mapping.Util
import com.hilabs.model.{AssociationModelDetails, AssociationModelXFeatures, Datasets, ModelParameters, ScenarioDetails}
import com.hilabs.products.process_validation.results.ProcessValidationResults
import com.hilabs.products.process_validation.utils.ProcessValidationParameters
import com.hilabs.products.utils._
import com.hilabs.service.impl.{AssociationModelXFeaturesServiceImpl, ScenarioDetailsServiceImpl}
import com.hilabs.solr.Spark_solr
import com.hilabs.util.generic.ScalaGenericUtility
import org.apache.log4j.Logger
import org.apache.spark.sql.DataFrame
import org.apache.spark.sql.functions.{col, trim, upper, when}
import com.hilabs.config.HilabsConfig
import com.hilabs.dao.impl.MysqlUpdate
import com.hilabs.dao.util.DatabaseConnector
import java.time.format.DateTimeFormatter
import java.time.LocalDateTime

import com.hilabs.products.process_validation.model_features.GetFeatureInfo
import com.hilabs.products.utils.logger.exceptions.PVException
import com.hilabs.products.utils.logger.notifications.ProcessValidationNotifications
import org.apache.spark.storage.StorageLevel
//connecting to HilabsDatabases
var conf = HilabsConfig.loadEnvironmentSpecificConfig("mysql");
var con = DatabaseConnector.connectToDatabase(conf);
import com.hilabs.model.Input

import com.google.gson.Gson
//models parameters input //4629 //4634 //4651:download func //4659, 4705, 4713, 4716: binary report
val gson = new Gson()
val input = gson.fromJson("""{"benchmarkAsAnd":"true", "liftThresh":"0.5", "liftExplainThresh":"2.0", "useMIS":"false","functionality":"AssociationMining","modelId":"4781","numberOfRows":"10","size":"10","outputFile":"npitest","L1divisionSize":"100","L2divisionSize":"2","algorithm":"drf","batchSize":"3","isGenerateRelationship":"false","isPeekValueNeeded":"false","prunedRuleMinLength":"2","prunedRuleMaxLength":"3","queryId":"20","allowedRuleDeviation":"0.001","explainThreshHighConf":"0.00","explainThreshLowConf":"1.01","assignRuleIdsToRows":"true","CMSHCCVersion":"pattern_frequency","generateLikelihood":"false","useNewRelationhipMethod":"true","isGenerateRelationship":"false","isGenerateFile":"false","startIndex":"3","fpgDsMinimumOccuranceCount":"1000","fpgTestMinimumOccuranceCount":"1000","rulesLimit":"1000","classesLimitOnColumnForDrivingFactor":"10","runValidationFlag":"false","runFluffDeletionFlag":"false","runTrainStratSamplingFlag":"false","dropHighNAColumns":"false","solrColumnName":"","fromColumn":"","requestFrom":"","treatDateAsCategorical":"true","runOnlyForCorrelatedCols":"false","printCounts":"false","pvModelType":"2","trimData":"true","pvExactMatchThreshold":"0.75","pvMappingThreshold":"0.75","pvContainsThreshold":"0.75","pvStartsWithThreshold":"0.75","testRequiresTrain":"2","showRulesAsBuckets":"true","runRefactoredCode":"true"}""",classOf[Input])
print("\nRunnning Provider Validation")
var providerValidationModel = new ProviderValidation(sparkConf)
var sparkConf = new SparkConf();
val SparkJobStatus = new SparkJobStatus(sparkConf);
SparkJobStatus.appJobEntry(input)
//assign input values to model
val AssignParameters = new AssignParameters(sparkConf);
val modelParameters = AssignParameters.assignParameters(input)

modelParameters.basePath_=(sparkConf.SEED_PATH + "asso_mining_" + modelParameters.modelId + "/")
//basePath is the path of the model's connection that is stored in parquet format when the connection is ingested.
//similarly here it's store the model files
var processValidationParameters = new ProcessValidationParameters(sparkConf)
var associationModelDetails: AssociationModelDetails = AssociationModelDetailsServiceImpl.getAssociationModelDetails(modelParameters.modelId);

var inputScenarios = associationModelDetails.getAsso_logical_columns
if (inputScenarios != "" && inputScenarios != null) {
    associationModelDetails.setAsso_logical_columns(associationModelDetails.getAsso_logical_columns.substring(1, associationModelDetails.getAsso_logical_columns.length -1).replaceAll("\"",""))
}
println("Model fetched with ID = " + associationModelDetails.getAssociationModelId);

//target scenario id
var scenarioId = associationModelDetails.getScenarioId;
var scenarioDetails = ScenarioDetailsServiceImpl.getScenarioDetails(scenarioId)

//manually assign source_scenario_ids
val source_scenario_ids = associationModelDetails.getAsso_logical_columns.split(",").map(_.toInt)
var resultList: List[Map[Int, DataFrame]]= scala.collection.immutable.List()
println("\nSource Scenarios Found: " + associationModelDetails.getAsso_logical_columns)
var source_scenario_id=

for (source_scenario_id <- source_scenario_ids) {
  var scenarioDetails = ScenarioDetailsServiceImpl.getScenarioDetails(source_scenario_id)
  println("\n\nFETCHING DATA FOR SOURCE SCENARIO: " + source_scenario_id + " , NAME: " + scenarioDetails.getScenarioName + "\n\n")

  //source - connection file not present/corrupt, scenario inactive, dataset inactive, connection inactive, dataset query not formed correctly
  //connection id, scenario id, data set id, file location in hadoop, dataset query
  println("\nRunning data transform for source:")
  var dataTransformSource = new com.hilabs.products.process_validation.dataprep.DataTransform(sparkConf)
  dataTransformSource.getAndTransformData(associationModelDetails, source_scenario_id, modelParameters)

  //target - connection file not present/corrupt, scenario inactive, dataset inactive, connection inactive, dataset query not formed correctly
  //connection id, scenario id, data set id, file location in hadoop, dataset query
  println("\nRunning data transform for target:")
  var dataTransformTarget = new com.hilabs.products.process_validation.dataprep.DataTransform(sparkConf)
  dataTransformTarget.getAndTransformData(associationModelDetails, associationModelDetails.getScenarioId, modelParameters)

  //testModelColNamesMap
  println("\nRunning featureInfo:")
  var featureInfo = new com.hilabs.products.process_validation.model_features.GetFeatureInfo(sparkConf)
  featureInfo.getFeatureInfo(associationModelDetails, modelParameters, dataTransformSource.connectionId, dataTransformTarget.connectionId, source_scenario_id,
    associationModelDetails.getScenarioId)
  println(featureInfo.toString())

  println("\nFetching learnings:")
  var fetchLearnings = new com.hilabs.products.process_validation.utils.pv_methods.GetLearnings(sparkConf)
  var learnings = fetchLearnings.getLearnings("TEST3", dataTransformSource.df, dataTransformTarget.df,
    dataTransformSource.connectionId, dataTransformTarget.connectionId, associationModelDetails.getReferenceModelId, processValidationParameters.learningsDirectory)

  val learningsArray = learnings.filter(col("ColumnKey").isin(featureInfo.testModelSolrCols: _*)).rdd.
    map(row => (row.getAs[String]("ColumnKey"), row.getAs[String]("ColumnValue"), row.getAs[String]("valuesMap"))).collect
  println("learningsArray: " + learningsArray.mkString(","))

  val learningsArrayWithKey = learnings.rdd.
    map(row => (row.getAs[String]("ColumnKey"), row.getAs[String]("ColumnValue"), row.getAs[String]("valuesMap"))).collect

  var learningsArrayMap = (learningsArrayWithKey.map(_._1)).zip(learningsArrayWithKey.map(_._2)).toMap
  println("learningsArrayMap: " + learningsArrayMap)

  println("\nGenerating key columns:")
  var keyCols = associationModelDetails.getTestKey.split(",").map(x => (x, learningsArrayMap.getOrElse(x,"NOT_FOUND")))
  if (keyCols.map(_._2).contains("NOT_FOUND")) {
    throw new PVException(ProcessValidationNotifications.generateNotificationWithText(s"Not able match column for key in source ${scenarioDetails.getScenarioName} (${source_scenario_id})", List(8)))
  }
  //keyCols is an array of(string, string) tuple which is a tuple of (targetkeysolrcolumn name, corresponding sourcekeysolrcolumn name)
  var exactKeyCols = keyCols.filter(x => !featureInfo.probSolrColumnDeviationThresholdMap.keys.toList.contains(x._1))
  var probKeyCols = keyCols.filter(x => featureInfo.probSolrColumnDeviationThresholdMap.keys.toList.contains(x._1) )
  var regularFunctions = new com.hilabs.products.process_validation.utils.generic_functions.RegularFunctions(sparkConf)

  if (modelParameters.ignoreEmptyKeys) {
    for ((targetCol, sourceCol) <- exactKeyCols) {
      dataTransformSource.df = dataTransformSource.df.filter(col(sourceCol).isNotNull)
      dataTransformTarget.df = dataTransformTarget.df.filter(col(targetCol).isNotNull)
    }
  }

  println("\nGenerating exact primary key for source:")
  var dfSource = regularFunctions.generatePrimaryKeyExact(dataTransformSource.df, exactKeyCols.map(_._2).mkString(","), processValidationParameters.primaryKey)
  println("\nGenerating exact primary key for target:")
  var dfTarget = regularFunctions.generatePrimaryKeyExact(dataTransformTarget.df, exactKeyCols.map(_._1).mkString(","), processValidationParameters.primaryKey)

  // Generating all probablistic columns
  var probKey = new com.hilabs.products.process_validation.utils.pv_methods.GenerateProbKeyComplete(sparkConf)
  var resTuple = probKey.generateProbKeys(dfSource, dfTarget, modelParameters, associationModelDetails,
    processValidationParameters, featureInfo, probKeyCols)
  dfSource = resTuple._1
  dfTarget = resTuple._2
  //persist data
  dfSource.persist(StorageLevel.MEMORY_AND_DISK)
  dfTarget.persist(StorageLevel.MEMORY_AND_DISK)

  println("\nGenerating rules:")
  var getRuleData = new com.hilabs.products.process_validation.results.GenerateRuleData(sparkConf)
  var tmpMap = getRuleData.generateRuleData(dfSource,dfTarget,modelParameters,associationModelDetails,processValidationParameters,featureInfo, learningsArray, learningsArrayMap)

  resultList ++= List(tmpMap)
    }

  println("\n\nALL SOURCES PROCESSED, JOINING DATA\n\n")
    //create sample featureInfo
    var source_scenario_id = source_scenario_ids(0)
    var loadData = new com.hilabs.products.utils.LoadData(sparkConf);
    val sourceConnectionId = loadData.getDfFromScenarioId(source_scenario_id)._2
    val targetConnectionId = loadData.getDfFromScenarioId(associationModelDetails.getScenarioId)._2
    println("\nRunning featureInfo:")
    var featureInfo = new com.hilabs.products.process_validation.model_features.GetFeatureInfo(sparkConf)
    featureInfo.getFeatureInfo(associationModelDetails, modelParameters, sourceConnectionId, targetConnectionId, source_scenario_id,
      associationModelDetails.getScenarioId)
    println(featureInfo.toString())

    println("\nAdding rules to db:")
    var processValidationResults = new com.hilabs.products.process_validation.results.ProcessValidationResults(sparkConf)


    var resultMap: Map[Int, DataFrame]= Map().empty
    var nullDf: DataFrame = null
    var ruleIdList = resultList(0).map(_._1)
    for (ruleId <- ruleIdList) {
      println("\nJoining data for rule: " + processValidationParameters.ruleTextMap.getOrElse(ruleId, ""))
      var getJoinData = new com.hilabs.products.process_validation.results.JoinData(sparkConf)
      var tmpDf = getJoinData.joinData(ruleId, resultList.map(x => x.getOrElse(ruleId,nullDf)),modelParameters,associationModelDetails,processValidationParameters)
      if (tmpDf != null) {
        Util.writeParquetFile(tmpDf, modelParameters.basePath + ruleId)
      }
      println("df count: " + tmpDf.count)
      resultMap = resultMap + (ruleId -> tmpDf)
    }

    println("\nGenerating Report, creating Indexing data and inserting in temp valuesets:")
